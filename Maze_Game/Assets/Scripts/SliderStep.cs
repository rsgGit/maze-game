using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderStep : MonoBehaviour
{

    int stepAmount = 10;
    public Slider mySlider = null;
    Text text;
    int numberOfSteps = 0;
    string size = "Size: ";

    // Start is called before the first frame update
    void Start()
    {
       // mySlider = GetComponent<Slider>();
        text = GetComponent<Text>();
        numberOfSteps = (int)mySlider.maxValue / stepAmount;
        text.text = size + mySlider.value;

    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void UpdateStep()
    {
        float range = (mySlider.value / mySlider.maxValue) * numberOfSteps;
        int ceil = Mathf.CeilToInt(range);
        mySlider.value = ceil * stepAmount;
        text.text = size + mySlider.value;
    }

}
