using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HoleTrigger : MonoBehaviour
{
    public Text message1;
    public Text message2;
    public GameObject maze;
  
    // Start is called before the first frame update
    void Start()
    {
       
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.name.Contains("RedBall"))
        {
     
            collision.transform.position = gameObject.transform.position;
            message1.enabled = true;
            message2.enabled = true;

            if (maze != null) maze.GetComponent<MazeGenerator>().destroyTransforms();
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
