using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MazeGenerator : MonoBehaviour
{

    struct Cell
    {
       public bool left, right, up, down, visited;
       public int xInMazeArray;
       public int yInMazeArray;
       public int xOnScreen;
       public int yOnScreen;
    }

   
    private int size;
    public Transform wall;
    public Transform redBall;
    public Transform hole;
    public Button generateButton;
    public Slider slider;
    public Text message1;
    public Text message2;
    private List<Transform> transforms = new List<Transform>();
    Cell[,] maze;

 

    // Start is called before the first frame update
    void Start()
    {
        message1.enabled = false; //hide messages
        message2.enabled = false;
        size = (int)slider.value; //get initial slider value
        Button btn = generateButton.GetComponent<Button>(); //init button
        btn.onClick.AddListener(TaskOnClick); //add listener
        createMazeWithCells(size);
        drawMaze(recursiveBackTrack(maze), size);
       
    }


    // Update is called once per frame
    void Update()
    {
    
        
    }

    /// <summary>
    /// Destroys all wall/ball/hole transforms
    /// </summary>
    public void destroyTransforms()
    {
        for (int i = 0; i < transforms.Count; i++)
        {
            Destroy(transforms[i].gameObject);
        }
        transforms.Clear();
    }


    /// <summary>
    /// on click button
    /// </summary>
    void TaskOnClick()
    {

        destroyTransforms();
        size = (int)slider.value; //get input slider values
        createMazeWithCells(size);
        drawMaze(recursiveBackTrack(maze), size);

    }

    /// <summary>
    /// Creates an empty maze with cells
    /// </summary>
    /// <param name="size"></param>
    /// <param name="size"></param>
    void createMazeWithCells(int size)
    {
        maze = new Cell[size, size];
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                Cell cell = new Cell();
                cell.left = true;
                cell.right = true;
                cell.up = true;
                cell.down = true;
                cell.visited = false;
                cell.xOnScreen = i - size / 2;
                cell.yOnScreen = j - size / 2;
                cell.xInMazeArray = i;
                cell.yInMazeArray = j;
                maze[i, j] = cell;

            }
        }

    }

  

    /// <summary>
    /// Draws the maze on screen
    /// </summary>
    /// <param name="maze"></param>
    /// <param name="size"></param>
    /// <param name="size"></param>
    void drawMaze(Cell[ ,] maze, int size)
    {
        message1.enabled = false;
        message2.enabled = false;
        for (int i=0; i<size; i++)
        {
            for(int j=0; j<size; j++)
            {
                Cell cell = maze[i,j];
                int x = i - (size / 2) - (size/4);
                int y = j - size / 2 ;

                if (cell.up)
                {
                    Transform drawingWall = Instantiate(wall, transform) as Transform;
                    drawingWall.position = new Vector2(x, y+0.5f);
                    transforms.Add(drawingWall);
                    
                }

                if (cell.left)
                {
                    
                    Transform drawingWall = Instantiate(wall, transform) as Transform;
                    drawingWall.position = new Vector2(x-0.5f, y);
                    drawingWall.eulerAngles = new Vector3(0, 0, 90);
                    transforms.Add(drawingWall);
                }

                if ((i == size - 1) && cell.right)
                {
                    Transform drawingWall = Instantiate(wall, transform) as Transform;
                    drawingWall.position = new Vector2(x + 0.5f, y);
                    drawingWall.eulerAngles = new Vector3(0, 0, 90);
                    transforms.Add(drawingWall);
                }

                if ((j == 0) && cell.down)
                {
                    Transform drawingWall = Instantiate(wall, transform) as Transform;
                    drawingWall.position = new Vector2(x, y - 0.5f);
                    transforms.Add(drawingWall);

                }


            }
        }

        var random = new System.Random();
        int x1 = random.Next(0, size);
        int y1 = random.Next(0, size);
        int x2 = size - x1 - 1;
        int y2 = size - y1 - 1;
        Camera.main.orthographicSize = size*0.6f;
        Transform ball = Instantiate(redBall, transform) as Transform;
        ball.position = new Vector2(x1 - (size / 2) - (size / 4), y1 - size / 2);
        Transform hole1 = Instantiate(hole, transform) as Transform;
        hole1.position = new Vector2(x2 - (size / 2) - (size / 4), y2 - size / 2);
        hole1.GetComponent<HoleTrigger>().message1 = message1;
        hole1.GetComponent<HoleTrigger>().message2 = message2;
        hole1.GetComponent<HoleTrigger>().maze = gameObject;
        transforms.Add(ball);
        transforms.Add(hole1);
    }

    
    /// <summary>
    /// Return all the unvisited neighbours
    /// </summary>
    /// <param name="cell"></param>
    /// <param name="maze"></param>
    /// <returns></returns>
    List<Cell> getUnvisitedNeighbours(Cell cell, Cell[ , ] maze)
    {
        List<Cell> cells = new List<Cell>();

        int i = cell.xInMazeArray;
        int j = cell.yInMazeArray;

        if (i > 0) //Left
        {
            Cell potentialNeighbour = maze[i - 1, j];
            if (!(potentialNeighbour.visited)) cells.Add(potentialNeighbour);
        }
        if (i < size-1) //Right
        {
            Cell potentialNeighbour = maze[i + 1, j];
            if (!(potentialNeighbour.visited)) cells.Add(potentialNeighbour);
        }
        if (j > 0) //Down
        {
            Cell potentialNeighbour = maze[i, j-1];
            if (!(potentialNeighbour.visited)) cells.Add(potentialNeighbour);
        }
        if (j < size-1) //Up
        {
            Cell potentialNeighbour = maze[i, j+1];
            if (!(potentialNeighbour.visited)) cells.Add(potentialNeighbour);
        }

        return cells;
    }

    

    /// <summary>
    /// return cells with removed walls
    /// </summary>
    /// <param name="cellA"></param>
    /// <param name="cellB"></param>
    /// <returns></returns>
    void removeWall(Cell cellA, Cell cellB, Cell[,] maze) {

        Cell a = cellA;
        Cell b = cellB;
        if (a.xInMazeArray == b.xInMazeArray)
        {
            if (a.yInMazeArray == b.yInMazeArray + 1) //A is ontop of B
            {
                a.down = false;
                b.up = false;
            }

            else if (a.yInMazeArray == b.yInMazeArray - 1) //A is below B
            {
                a.up = false;
                b.down = false;
            }
        }

        else if (a.yInMazeArray == b.yInMazeArray)
        {
            if (a.xInMazeArray == b.xInMazeArray + 1) //A is right of B
            {
                a.left = false;
                b.right = false;
            }

            else if (a.xInMazeArray == b.xInMazeArray - 1) //A is left of B
            {
                a.right = false;
                b.left = false;
            }
        }
        b.visited = true;
        maze[a.xInMazeArray, a.yInMazeArray] = a;
        maze[b.xInMazeArray, b.yInMazeArray] = b;

    }


   
    /// <summary>
    /// Algorithm that generates the maze
    /// </summary>
    /// <param name="maze"></param>
    /// <returns></returns>
    Cell[,] recursiveBackTrack(Cell [,] maze)
    {

        Cell[,] newMaze = maze; //modified maze
        Stack<Cell> pathWay = new Stack<Cell>(); //stack to keep note of path for backtracking
        var random = new System.Random(); //generate random seed
        int x = random.Next(0, size); //find random x for start point
        int y = random.Next(0, size); //find random y for start point
        newMaze[x, y].visited = true; //set start point as visited
        Cell current = newMaze[x, y]; //initialize current cell 
        pathWay.Push(current); //push this into the stack
        while ((pathWay.Count!=0))
         {
            
            current = pathWay.Pop(); //pop from stack
            current = newMaze[current.xInMazeArray, current.yInMazeArray]; //update the current cell
             List<Cell> unvisitedNeighbours = getUnvisitedNeighbours(current, newMaze); //get its unvisited neighbours
        
            if (unvisitedNeighbours.Count != 0) //if there are neighbours
            {
                pathWay.Push(current); //push it back into stack  
                int randomIndex = random.Next(0, unvisitedNeighbours.Count); //choose random neighbour
                Cell next = unvisitedNeighbours[randomIndex]; //get the cell of the random neighbour
                removeWall(current, next, newMaze); //remove wall and mark neighbour visited
                current = newMaze[current.xInMazeArray, current.yInMazeArray]; //update current cell after removing wall
                next = newMaze[next.xInMazeArray, next.yInMazeArray]; //update next cell after removing wall
                current = next; //change current to be next
                pathWay.Push(current); //push the current (next) cell into the stack
                
            }

          
        }
        
        return newMaze;
    }

    

}
