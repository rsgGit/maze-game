﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void HoleTrigger::Start()
extern void HoleTrigger_Start_mCE7BF1552A407D63E130248AC2804F1529D2F340 (void);
// 0x00000002 System.Void HoleTrigger::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void HoleTrigger_OnTriggerEnter2D_m254F2230F73CB49A87FB5A3B586858E9E0F4AFB3 (void);
// 0x00000003 System.Void HoleTrigger::Update()
extern void HoleTrigger_Update_mC90DC0D9DDD1D90315EC69235B0CA8FF1BA1AC8A (void);
// 0x00000004 System.Void HoleTrigger::.ctor()
extern void HoleTrigger__ctor_m1A870A8BDC78823EC02A03495DFA53BAD8984048 (void);
// 0x00000005 System.Void MazeGenerator::Start()
extern void MazeGenerator_Start_mD73B9A709F2E28D74587DD4501488000CC214451 (void);
// 0x00000006 System.Void MazeGenerator::Update()
extern void MazeGenerator_Update_mC97367094A4FA73F0373C8280EA23466E3F53E04 (void);
// 0x00000007 System.Void MazeGenerator::destroyTransforms()
extern void MazeGenerator_destroyTransforms_m398E86FB7F205CB3792D5AA2D1928D25AB17343D (void);
// 0x00000008 System.Void MazeGenerator::TaskOnClick()
extern void MazeGenerator_TaskOnClick_mD273F7267CBE01B87EC8562EAFD445BD9D9957CC (void);
// 0x00000009 System.Void MazeGenerator::createMazeWithCells(System.Int32)
extern void MazeGenerator_createMazeWithCells_mA9225BB5B869ED8A56AF18410BC5D43B3BE731EA (void);
// 0x0000000A System.Void MazeGenerator::drawMaze(MazeGenerator/Cell[0...,0...],System.Int32)
extern void MazeGenerator_drawMaze_m30AE2FE5D2E7B98C381C3F624816BD398BB9B62B (void);
// 0x0000000B System.Collections.Generic.List`1<MazeGenerator/Cell> MazeGenerator::getUnvisitedNeighbours(MazeGenerator/Cell,MazeGenerator/Cell[0...,0...])
extern void MazeGenerator_getUnvisitedNeighbours_mC1135127E87F0376C3A60677F8D2923005CFA42D (void);
// 0x0000000C System.Void MazeGenerator::removeWall(MazeGenerator/Cell,MazeGenerator/Cell,MazeGenerator/Cell[0...,0...])
extern void MazeGenerator_removeWall_m247CBACCCA9B8DD9679EE261A0B72B67C16510A6 (void);
// 0x0000000D MazeGenerator/Cell[0...,0...] MazeGenerator::recursiveBackTrack(MazeGenerator/Cell[0...,0...])
extern void MazeGenerator_recursiveBackTrack_m870B630F2E2398DF83FFD56032B419B6A99FB25B (void);
// 0x0000000E System.Void MazeGenerator::.ctor()
extern void MazeGenerator__ctor_mCF96DF656CA450CBADF245C3A79373E1FCEBE43A (void);
// 0x0000000F System.Void RedBallMovement::Start()
extern void RedBallMovement_Start_m23D4BBC712CAAA49EDA9B2FA95F57000A3CB3C86 (void);
// 0x00000010 System.Void RedBallMovement::Update()
extern void RedBallMovement_Update_m07A0E1E29B8885FCA8BE42004B4436459916D6FC (void);
// 0x00000011 System.Void RedBallMovement::.ctor()
extern void RedBallMovement__ctor_m8B74E41BDA763B46EA532A7EC52BA957D7F0A761 (void);
// 0x00000012 System.Void SliderStep::Start()
extern void SliderStep_Start_m73BCEA562F5EBAE484BEF801B22D463094D5EEF5 (void);
// 0x00000013 System.Void SliderStep::Update()
extern void SliderStep_Update_m6ACFEE6ACE2B00A7605A7D3218C35126557A8DB1 (void);
// 0x00000014 System.Void SliderStep::UpdateStep()
extern void SliderStep_UpdateStep_m2E1F76B2AC1D98D2F002F54D6291130BEA85CC55 (void);
// 0x00000015 System.Void SliderStep::.ctor()
extern void SliderStep__ctor_m7ADCAC2DDFF67769AA855CB5E169BCAAB9100FCD (void);
static Il2CppMethodPointer s_methodPointers[21] = 
{
	HoleTrigger_Start_mCE7BF1552A407D63E130248AC2804F1529D2F340,
	HoleTrigger_OnTriggerEnter2D_m254F2230F73CB49A87FB5A3B586858E9E0F4AFB3,
	HoleTrigger_Update_mC90DC0D9DDD1D90315EC69235B0CA8FF1BA1AC8A,
	HoleTrigger__ctor_m1A870A8BDC78823EC02A03495DFA53BAD8984048,
	MazeGenerator_Start_mD73B9A709F2E28D74587DD4501488000CC214451,
	MazeGenerator_Update_mC97367094A4FA73F0373C8280EA23466E3F53E04,
	MazeGenerator_destroyTransforms_m398E86FB7F205CB3792D5AA2D1928D25AB17343D,
	MazeGenerator_TaskOnClick_mD273F7267CBE01B87EC8562EAFD445BD9D9957CC,
	MazeGenerator_createMazeWithCells_mA9225BB5B869ED8A56AF18410BC5D43B3BE731EA,
	MazeGenerator_drawMaze_m30AE2FE5D2E7B98C381C3F624816BD398BB9B62B,
	MazeGenerator_getUnvisitedNeighbours_mC1135127E87F0376C3A60677F8D2923005CFA42D,
	MazeGenerator_removeWall_m247CBACCCA9B8DD9679EE261A0B72B67C16510A6,
	MazeGenerator_recursiveBackTrack_m870B630F2E2398DF83FFD56032B419B6A99FB25B,
	MazeGenerator__ctor_mCF96DF656CA450CBADF245C3A79373E1FCEBE43A,
	RedBallMovement_Start_m23D4BBC712CAAA49EDA9B2FA95F57000A3CB3C86,
	RedBallMovement_Update_m07A0E1E29B8885FCA8BE42004B4436459916D6FC,
	RedBallMovement__ctor_m8B74E41BDA763B46EA532A7EC52BA957D7F0A761,
	SliderStep_Start_m73BCEA562F5EBAE484BEF801B22D463094D5EEF5,
	SliderStep_Update_m6ACFEE6ACE2B00A7605A7D3218C35126557A8DB1,
	SliderStep_UpdateStep_m2E1F76B2AC1D98D2F002F54D6291130BEA85CC55,
	SliderStep__ctor_m7ADCAC2DDFF67769AA855CB5E169BCAAB9100FCD,
};
static const int32_t s_InvokerIndices[21] = 
{
	1373,
	1128,
	1373,
	1373,
	1373,
	1373,
	1373,
	1373,
	1119,
	669,
	484,
	414,
	863,
	1373,
	1373,
	1373,
	1373,
	1373,
	1373,
	1373,
	1373,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	21,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
